The repository includes public PowerShell scripts for SharePoint and more applications in the future. All scripts were tested for the applications/systems mentioned in the readme files. For documentation read the readme files and visit the link in the script description for updates.  

**Bitbucket** - https://bitbucket.org/TobiasAT    
**About.me** - https://about.me/tasboeck    
**Twitter** - https://twitter.com/tasboeck 


