
<#  
    .DESCRIPTION  
    Add a new (SharePoint) content database to a SQL server. 
    Note, the function includes configurations for SharePoint environments and content databases.

    .Link 
    Read the description and updates at http://go.topedia.com/4kvrs.

   .Author: Tobias Asböck
   .Updated: 12.12.2016 
#> 


Function New-SQLDatabase
{ param(
[parameter(Mandatory=$true)]
[string]$SQLServer,

[parameter(Mandatory=$true)]
[string]$DBName,

[ValidateScript({$_ -gt 0} )]
[int]$SecondaryDataFiles = 1
)

# Loading the SQL Powershell module
Write-Host "Loading SQL Powershell objects..."
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Smo")  | out-null
Write-Host "Connecting to SQL Server..."
try{ $SQLServerObj = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Server -ArgumentList $SQLServer }
catch { Write-Host "No SQL module is available on that server, script canceled." -foregroundcolor Red; return }

# Checks whether the database is already created
if(($SQLServerObj.Databases | ?{$_.name -eq $DBName }) -eq $null)
{   Write-Host "Adding the database $DBName..."

    # Define the database options
    $NewDatabase = new-object ('Microsoft.SqlServer.Management.Smo.Database') ($SQLServerObj, $DBName)    
    $NewDatabase.Collation = "Latin1_General_CI_AS_KS_WS"
    $NewDatabase.RecoveryModel = 1 
    $NewDatabase.CompatibilityLevel = 100
    $NewDatabase.AnsiNullDefault = $false
    $NewDatabase.AnsiNullsEnabled = $false
    $NewDatabase.AnsiPaddingEnabled = $false
    $NewDatabase.AnsiWarningsEnabled = $false
    $NewDatabase.ArithmeticAbortEnabled = $false
    $NewDatabase.AutoClose = $false
    $NewDatabase.AutoCreateStatisticsEnabled = $true  
    $NewDatabase.AutoShrink = $false
    $NewDatabase.AutoUpdateStatisticsEnabled = $true
    $NewDatabase.CloseCursorsOnCommitEnabled = $false
    $NewDatabase.LocalCursorsDefault = $false
    $NewDatabase.ConcatenateNullYieldsNull = $false
    $NewDatabase.NumericRoundAbortEnabled  = $false
    $NewDatabase.QuotedIdentifiersEnabled  = $false
    $NewDatabase.RecursiveTriggersEnabled = $false
    $NewDatabase.BrokerEnabled = $false
    $NewDatabase.AutoUpdateStatisticsAsync  = $false
    $NewDatabase.DateCorrelationOptimization = $false
    $NewDatabase.IsParameterizationForced = $false
    $NewDatabase.ReadOnly = $false
    $NewDatabase.PageVerify  = "Checksum"
    $NewDatabase.UserAccess  = "Multiple"

    # Create the filegroup and primary data file
    $PrimaryFileGroup = new-object ('Microsoft.SqlServer.Management.Smo.FileGroup') ($NewDatabase, 'PRIMARY')
    $NewDatabase.FileGroups.Add($PrimaryFileGroup)

    $DataFileName = $DBName + "_mdf1"
	$DataFilePath = $SQLServerObj.DefaultFile
    $DataFile = new-object ('Microsoft.SqlServer.Management.Smo.DataFile') ($PrimaryFileGroup, $DataFileName)
    $DataFile.FileName = $DataFilePath + $DataFileName + ".mdf"
    $DataFile.Size = 1310720
    $DataFile.GrowthType = "KB"
    $DataFile.Growth = 1310720
    $DataFile.IsPrimaryFile = $true
    $PrimaryFileGroup.Files.Add($DataFile)

    # Create the secondary data files
    $FileCount = 2
    while($FileCount -le $SecondaryDataFiles)
    { $DataFileName = $DBName + "_ndf$FileCount"

    $DataFile = new-object ('Microsoft.SqlServer.Management.Smo.DataFile') ($PrimaryFileGroup, $DataFileName)
    $DataFile.FileName =  $DataFilePath + $DataFileName + ".ndf"
    $DataFile.Size = 1310720
    $DataFile.GrowthType = "KB"
    $DataFile.Growth = 1310720
    $DataFile.IsPrimaryFile = $false
    $PrimaryFileGroup.Files.Add($DataFile)
    $FileCount++
    }

    # Create the log file
    $LogFileName = $DBName + "_log"
	$LogFilePath = $SQLServerObj.DefaultLog
    $LogFile = new-object ('Microsoft.SqlServer.Management.Smo.LogFile') ($NewDatabase, $LogFileName)
    $LogFile.FileName = $LogFilePath + $LogFileName + '.ldf'
    $LogFile.Size = 2621440
    $LogFile.GrowthType = "KB"
    $LogFile.Growth = 655360
    $NewDatabase.LogFiles.Add($LogFile)

    # Create the new database
    try{ $NewDatabase.Create() }
    catch { Write-Host "ERROR during the database creation." -foregroundcolor Red; $Error[0].Exception.Message }    

    $NewDatabase = $SQLServerObj.Databases | ?{$_.name -eq $DBName }
    if($NewDatabase -ne $null)
    {   # Change the database owner to sa
        $NewDatabase.SetOwner("sa")
        $NewDatabase.Alter()
        $NewDatabase
        # Write-Host ("Database " + $NewDatabase.Name + " created.") -foregroundcolor Green
    }  
}
else { Write-Host "A database with the $DBName is already created." }
}
