Add a new (SharePoint) content database to a SQL server. 
-
#### **Command**   
New-SQLDatabase

#### Compatibility (tested) 	
- PowerShell 4
- SharePoint 2013
- SQL Server 2008 / 2012

#### **PARAMETER SQLServer**
The name + instance or alias of the SQL server. The parameter is required.

#### **PARAMETER DBName**
The name of the database. The parameter is required.

#### **PARAMETER SecondaryDataFiles**  
The number of secondary data files. The default value is 1.

#### **Example**   
New-SQLDatabase -SQLServer "Dev1" -DBName "Tobias1" -SecondaryDataFiles 8

#### **Remarks**  
The function includes configurations for SharePoint environments and content databases.

