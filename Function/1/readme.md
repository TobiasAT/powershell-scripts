Get information for a quota template or the quota of a site. 
-
#### **Command**   
Get-SPQuota 

#### Compatibility  	
- SharePoint 2010+
- PowerShell 2+  

#### **PARAMETER Name**
Defines the template name.

#### **PARAMETER SiteUrl**
Defines the site url.

#### **Example:**   
Get-SPQuota -Name "Site Quota 5000 MB"  
Get-SPQuota -SiteUrl "https://community.asbock.com/team1"