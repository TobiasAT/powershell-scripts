<#  
    .DESCRIPTION  
    Get the information for a quota template or the quota for a site. 

    .Link 
    Read the description and updates at http://go.topedia.com/FWUXa.

   .Author: Tobias Asböck
   .Updated: 25.07.2016  
#>  


Function Get-SPQuota 
{  param( [string]$Name = "", 
[string]$SiteUrl = "" )

$CAService = [Microsoft.SharePoint.Administration.SPWebService]::ContentService
if($Name -ne ""){   
    $Template = $CAService.QuotaTemplates[$Name]
    if($Template -ne $null)
        { $Template }
    else 
        { Write-Host "No quota template $Name available." -ForegroundColor Red }
}
elseif($SiteUrl -ne "")
{   $Site = Get-SPSite $SiteUrl -ErrorAction SilentlyContinue
    if($Site)
    {   if($Site.Quota.QuotaID -eq 0 )
        {   Write-Host "Custom quota defined:"; $Site.Quota }
        else 
        {   $TemplateName = ($CAService.QuotaTemplates | ?{$_.QuotaId -eq $site.Quota.QuotaID }).Name; $TemplateName }
    }
    else { Write-Host "No site $SiteUrl available." -ForegroundColor Red }
}
else { Write-Host "Please define the parameter -Name or -SiteUrl." }
}

