Send a mail from the SharePoint server. 
-
#### **Command**   
Send-SPFarmMail 

#### Compatibility (tested) 	
- PowerShell 4
- SharePoint 2013

#### **PARAMETER MailText**
Mail body in HTML format. The parameter is required.

#### **PARAMETER MailSubject**
Subject of the mail. The parameter is required.

#### **PARAMETER Recipient**  
The recipient(s) of the mail (TO). Add the values in an array. The parameter is required.

#### **PARAMETER RecipientCC**  
The recipient(s) of the mail (CC). Add the values in an array.

#### **Example**   
Send-SPFarmMail -MailSubject "Welcome" -MailText "A message"  -Recipient "someone@example.com" 

#### **Remarks**  
The SMTP-server is the outgoing mail server of the SharePoint farm.

