
<#  
    .DESCRIPTION  
    Send a mail from the SharePoint server. The SMTP-server is the outgoing mail server of the SharePoint farm.

    .Link
    Read the description and updates at http://go.topedia.com/0NRhF.

   .Author: Tobias Asböck
   .Updated: 13.12.2016    
#>  


function Send-SPFarmMail 
{   param( [parameter(Mandatory=$true)]
    [string]$Mailtext,

    [parameter(Mandatory=$true)]
    [String]$MailSubject,

    [parameter(Mandatory=$true)]
    [array]$Recipient,
    [array]$RecipientCC

    )

    if(Get-PSSnapin Microsoft.SharePoint.PowerShell -ErrorAction silentlycontinue)
    {   $CAWebApp = (Get-SPWebApplication -IncludeCentralAdministration) | ? { $_.IsAdministrationWebApplication -eq $true }
        $SenderAdress =  $CAWebApp.OutboundMailSenderAddress
        $SMTPServer = $CAWebApp.OutboundMailServiceInstance.Server.Address

        if($SMTPServer -ne $null)
        { if($RecipientCC -eq $null)
            { Send-MailMessage -From $SenderAdress -To $Recipient  -Subject $MailSubject  -Body ([string]$MailText) -SmtpServer $SMTPServer -BodyAsHtml }
        else 
            { Send-MailMessage -From $SenderAdress -To $Recipient -CC $RecipientCC  -Subject $MailSubject  -Body ([string]$MailText) -SmtpServer $SMTPServer -BodyAsHtml }  
        }
        else { Write-Host "No outgoing mail server has been configured." -f red }        
    }
    else { Write-Host "Please add the PowerShell snap-in." }
}
