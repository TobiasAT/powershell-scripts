Remove a site administrator from a site collection. 
-
#### **Command**   
Remove-SPSiteAdministrator 

#### Compatibility  	
- SharePoint 2010+
- PowerShell 2+

#### **PARAMETER WebUrl**
The URL of the site. The parameter is required.

#### **PARAMETER Username**
The loginname of the user. The parameter is required.

#### **Example:**   
Remove-SPSiteAdministrator -WebUrl "https://community.topedia.com/teamsite1" -Username "Domain\Username"
