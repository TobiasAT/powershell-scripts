<#  
    .DESCRIPTION  
    Remove a site administrator from a site collection. 

    .Parameter WebUrl
    The URL of the site. The parameter is required.

    .Parameter Username
    The loginname of the user. The parameter is required.

    .Link 
    http://go.topedia.com/x1ICN

   .Author: Tobias Asböck - https://bitbucket.org/TobiasAT
   .Updated: 09.07.2016  
#>  

Function Remove-SPSiteAdministrator 
{  param(
[parameter(Mandatory=$true)]
[string]$WebUrl,

[parameter(Mandatory=$true)]
[string]$Username
)

    $Web = Get-SPWeb $WebUrl
    $WebUser = Get-SPUser $Username -Web $Web
    $WebUser.IsSiteAdmin = $false
    $WebUser.Update()

}