
<#  
    .DESCRIPTION  
    Move the workflows of a SharePoint Site Collection to another Nintex content database.

    .Link
    Read the description and updates at http://go.topedia.com/E4IHi.

   .Author: Tobias Asböck
   .Updated: 19.08.2016    
#>  

function Move-NintexWorkflow
{ param(
[parameter(Mandatory=$true)]
[string]$SiteUrl,

[parameter(Mandatory=$true)]
[string]$NintexDatabaseName
)

# Check whether all functions are available
if (!(Get-Command Use-Assembly -errorAction SilentlyContinue))
	{ Write-Host "The command Use-Assembly is not available. Please load the command at http://go.topedia.com/5CTGt." -Foregroundcolor Red; $AbortScript = $true }

if (!(Get-Command Get-SQLDatabase -errorAction SilentlyContinue))
	{ Write-Host "The command Get-SQLDatabase is not available. Please load the command at http://go.topedia.com/lY65I." -Foregroundcolor Red; $AbortScript = $true }

if($AbortScript)
{ return }

Write-Host "Loading some information..."
# Get the site collection information
try{ $Site = Get-SPSite $SiteUrl }
catch { Write-Host "Site $Site not available." -foregroundcolor Red; return }

Use-Assembly -Name "Nintex.Workflow.dll"
$NintexConfigDB = [Nintex.Workflow.Administration.ConfigurationDatabase]::GetConfigurationDatabase()
$NintexContentDBOld = $NintexConfigDB.GetSiteCollectionStorage() | ?{$_.siteid -eq $site.id.guid }

# Get the Nintex database
$NintexContentDatabase = $NintexConfigDB.ContentDatabases.FindByDatabaseAndServerName($NintexConfigDB.SQLConnectionString["Data Source"],$NintexDatabaseName)
$NintexConfigDBName = $NintexConfigDB.SQLConnectionString["Initial Catalog"]

# Load the SQL database for Nintex
$Database = Get-SQLDatabase -Name $NintexConfigDBName -Server $NintexConfigDB.SQLConnectionString["Data Source"]
$NintexSQLDB = $Database.ExecuteWithResults("SELECT * FROM $NintexConfigDBName.dbo.ContentDbMapping WHERE NWContentDbId = '" + $NintexContentDatabase.DatabaseId + "'")

# If the mapping between the SharePoint and Nintex content database is not correct the database mapping in the Nintex database setup overview is not updated (_admin/NintexWorkflow/ViewDatabaseMappings.aspx).
# Update the mapping before you migrate the workflow. 
if($NintexSQLDB.Tables.SPContentDbId -eq $null )
	{ Write-Host "The SharePoint content database is mapped to an invalid Nintex content database. Please change the mapping." -foregroundcolor Red; $site.Dispose(); return }

# Validate that the workflows are not already in the database
$NintexContentDatabaseName = $NintexContentDatabase.SqlConnectionString["Initial Catalog"]
$NintexWorkflowData = $Database.ExecuteWithResults("SELECT top 3 * FROM $NintexContentDatabaseName.dbo.WorkflowInstance WHERE Siteid = '" + $site.id.guid + "'")
if($NintexWorkflowData.tables.rows.count -ne 0)
	{ Write-Host "The workflows were already moved and they are included in the database $NintexDatabaseName."; $site.Dispose(); return  }

$DatabaseServer = ($NintexConfigDB.ContentDatabases | ?{$_.databaseid -eq $NintexContentDBOld.ContentDatabaseId }).SqlConnectionString["Data Source"]
$Database = Get-SQLDatabase -Name $NintexDatabaseName -Server $DatabaseServer
if($Database -eq $null)
	{ return }

# Move the workflow to the other Nintex content database
Write-Host ("Moving the Nintex workflows to database " +  $Database.Name + "...")
$SourceConnectionString = $NintexContentDBOld.Connectionstring
$TargetConnectionString = ([String]::Format("Data Source=$DatabaseServer;Initial Catalog=" + $Database.Name + ";Integrated Security=True;"))

if($site.readonly -eq $true)
{ 	$SiteStatusReadOnly = $true 
	$SiteStatusLockIssue = $site.LockIssue
}

$site.Archived = $false # Unlock the site temporary to modify the lock message
$site = Get-SPSite $site.url
$site.LockIssue = "Locked for Nintex workflow migration"
$site.Archived = $true

NWAdmin.exe -o MoveData -Url $site.Url -TargetDatabase $TargetConnectionString -SourceDatabase $SourceConnectionString # | out-null
$Time = (Get-Date).AddMinutes(10)
Write-Host ("To complete the movement the script has to wait until " + (Get-Date $Time -format T) + "...")
Start-Sleep -Seconds 600

$site.Archived = $false
$site.LockIssue = $null

# Disable/Enable the Nintex site feature to update the "visual information" in the database mapping overview
Disable-SPFeature "NintexWorkflow" -Url $site.url -Confirm:$false
Enable-SPFeature "NintexWorkflow" -url $Site.url

# if the site was locked lock it again
if($SiteStatusReadOnly -eq $true)
	{ $site.LockIssue = $SiteStatusLockIssue; $site.Archived = $true }

$site.Dispose()
Write-Host "Workflow move completed." -foregroundcolor Green
}
