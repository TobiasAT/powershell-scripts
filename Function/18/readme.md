Move the workflows of a SharePoint Site Collection to another Nintex content database.
-
#### **Command**   
Move-NintexWorkflow

#### Compatibility (tested) 	
- PowerShell 4
- SharePoint 2013
- Nintex Workflow 2013

#### **PARAMETER SiteUrl**
Url of the SharePoint Site Collection. The parameter is required.

#### **PARAMETER NintexDatabaseName**
The name of the Nintex database. The workflows of the site will be migrated to that database. The parameter is required. 


#### **Example**   
Move-NintexWorkflow -SiteUrl "https://example.topedia.com/team10" -NintexDatabaseName "NintexContentDatabase1"
