<#  
    .DESCRIPTION  
    Add a new site administrator to a site collection. 

    .Parameter WebUrl
    The URL of the site. The parameter is required.

    .Parameter Username
    The loginname of the user. The parameter is required.

    .Link 
    http://go.topedia.com/9P2lS

   .Author: Tobias Asböck - https://bitbucket.org/TobiasAT
   .Updated: 09.07.2016  
#>  


Function Add-SPSiteAdministrator 
{  param(
[parameter(Mandatory=$true)]
[string]$WebUrl,

[parameter(Mandatory=$true)]
[string]$Username
)

    $Web = Get-SPWeb $WebUrl
    $WebUser = Get-SPUser $Username -Web $Web -erroraction silentlycontinue
    if($WebUser -eq $null)
       { $WebUser = New-SPUser -UserAlias $Username -Web $Web }
    $WebUser.IsSiteAdmin = $true
    $WebUser.Update()
}
