Add a new site administrator to a site collection.  
-
#### **Command**   
Add-SPSiteAdministrator 

#### Compatibility  	
- SharePoint 2010+
- PowerShell 2+

#### **PARAMETER WebUrl**
The URL of the site. The parameter is required.

#### **PARAMETER Username**
The loginname of the user. The parameter is required.

#### **Example:**   
Add-SPSiteAdministrator -WebUrl "https://community.topedia.com/teamsite1" -Username "Domain\Username"
