<#  
    .DESCRIPTION  
    Change the mapping of a SharePoint and Nintex content database.

    .Link
    Read the description and updates at http://go.topedia.com/4KpsZ.

   .Author: Tobias Asböck
   .Updated: 19.08.2016    
#>  

function Update-NintexDatabaseMapping
{ param(
[parameter(Mandatory=$true)]
[string]$NintexDatabaseName,

[parameter(Mandatory=$true)]
[string]$SharePointDatabaseName,
[string]$DatabaseServer

)

# Check whether all functions are available
if (!(Get-Command Use-Assembly -errorAction SilentlyContinue))
    { Write-Host "The command Use-Assembly is not available. Please load the command at http://go.topedia.com/5CTGt." -Foregroundcolor Red; $AbortScript = $true }

if($AbortScript)
{ return }

# Loading Nintex and SQL Powershell assemblies
Use-Assembly -Name "Nintex.Workflow.dll"
Use-Assembly -Name "Microsoft.SqlServer.Smo.dll"

Write-Host "Getting Nintex content database information..."
$NintexConfigDB = [Nintex.Workflow.Administration.ConfigurationDatabase]::GetConfigurationDatabase()
try{ $SPContentDatabase = Get-SPContentDatabase $SharePointDatabaseName }
catch { Write-Host "SharePoint content database $SharePointDatabaseName not available." -foregroundcolor Red; return }

if($DatabaseServer -eq $null)
    { $DatabaseServer = $SPContentDatabase.Server }

$SQLServerObj = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Server -ArgumentList $DatabaseServer
$NintexContentDatabase = $SQLServerObj.Databases[$NintexDatabaseName]  

if($NintexContentDatabase -ne $null )   
{   # Load the Nintex information from the SQL server. 
    # Unfortunately the Nintex assembly works just for one new database and is blocked to get additional (new) databases. The database information is available in the SQL database.
    $NintexConfigDBName = $NintexConfigDB.SQLConnectionString["Initial Catalog"]
    $NintexContentDB = $NintexContentDatabase.ExecuteWithResults("SELECT * FROM $NintexConfigDBName.dbo.Databases WHERE DatabaseName = '" + $NintexContentDatabase.Name + "'")

    Write-Host "Updating the Nintex content database mapping..." 
    [Nintex.Workflow.ContentDbMapping]$NintexContentDBMapping;
    $NintexContentDBMapping = [Nintex.Workflow.ContentDbMappingCollection]::ContentDbMappings.GetContentDbMappingForSPContentDb($SPContentDatabase.id)            
    # if there are no content database mappings found, initialise a new ContentDatabaseMapping object            
    if($NintexContentDBMapping -eq $null)            
        { $NintexContentDBMapping = New-Object Nintex.Workflow.ContentDbMapping }            
    # Set the properties of the ContentDatabaseMapping object to associate the Nintex Content Database with the SharePoint content database           
    $NintexContentDBMapping.SPContentDbId = $SPContentDatabase.id            
    $NintexContentDBMapping.NWContentDbId = $NintexContentDB.Tables.DatabaseID 
    try{ $NintexContentDBMapping.CreateOrUpdate() | out-null }
    catch{ Write-Host "Error to update the current mapping." -foregroundcolor Red  }
}
else 
    {Write-Host "Nintex content database $NintexDatabaseName not available." -foregroundcolor Red }
}