Change the mapping of a SharePoint and Nintex content database.
-
#### **Command**   
Update-NintexDatabaseMapping

#### Compatibility (tested) 	
- PowerShell 4
- SharePoint 2013
- Nintex Workflow 2013

#### **PARAMETER NintexDatabaseName**
The name of the Nintex database. The parameter is required.  

#### **PARAMETER SharePointDatabaseName**
The name of the SharePoint database. The parameter is required.  


#### **Example**   
Update-NintexDatabaseMapping -NintexDatabaseName "NintexContentDatabase1" -SharePointDatabaseName "SharePointContentDatabase1"
