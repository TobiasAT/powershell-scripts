Bitbucket users with an account get a search window if they enter a . on the keyboard. 
Non Bitbucket users are not able to use that option. I list all commands below, please use the search of your browser (CTRL + F). 

[Add-SPGroupMember](/Function/8/?at=master)  
[Add-SPGroupPermission](/Function/7/?at=master)  
[Add-SPSiteAdministrator](/Function/3/?at=master)  
[Enable-O365License](/Function/46/?at=master)  
[Get-SPList](/Function/20/?at=master) 
[Get-SPQuota](/Function/1/?at=master)  
[Get-SQLDatabase](/Function/15/?at=master)   
[Move-NintexWorkflow](/Function/18/?at=master)  
[New-NintexSQLDatabase](/Function/17/?at=master)  
[New-SPQuotaTemplate](/Function/2/?at=master)  
[New-SQLDatabase](/Function/11/?at=master)  
[Remove-SPSiteAdministrator](/Function/4/?at=master)  
[Send-SPFarmMail](/Function/19/?at=master)  
[Stop-SPPermissions](/Function/5/?at=master)  
[Update-NintexDatabaseMapping](/Function/16/?at=master)   
[Update-SPQuotaTemplate](/Function/30/?at=master)   
[Use-Assembly](/Function/14/?at=master) 



 