Create a new Nintex (content) database.
-
#### **Command**   
New-NintexSQLDatabase

#### Compatibility (tested) 	
- PowerShell 4
- Nintex Workflow 2013
- SQL Server 2008
- SQL Server 2012

#### **PARAMETER Name**
Name of the new database. The parameter is required.  

#### **PARAMETER Server**
Name of the SQL server. The parameter is required.  


#### **Example**   
New-NintexSQLDatabase -Name "NintexContentDatabase1" -Server "SQL1"
