<#  
    .DESCRIPTION  
    Create a new Nintex (content) database.

    .Link
    Read the description and updates at http://go.topedia.com/fpVGO.

   .Author: Tobias Asböck
   .Updated: 19.08.2016    
#>  

Function New-NintexSQLDatabase
{ param( 
    [parameter(Mandatory=$true)]
    [string]$Name,

    [parameter(Mandatory=$true)]
    [string]$Server
)

if (!(Get-Command Use-Assembly -errorAction SilentlyContinue))
    { Write-Host "The command Use-Assembly is not available. Please load the command at http://go.topedia.com/5CTGt." -Foregroundcolor Red; return }

# Loading the SharePoint and SQL Powershell modules
Use-Assembly -Name "Microsoft.SqlServer.Smo.dll"
Add-PSSnapin Microsoft.SharePoint.PowerShell -ErrorAction stop

# Connecting to SQL Server
try{ $SQLServerObj = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Server -ArgumentList $Server }
catch { Write-Host "No SQL module is available on that server, script canceled." -foregroundcolor Red; return }

# Checks whether the database is already created
if(($SQLServerObj.Databases | ?{$_.name -eq $Name }) -eq $null)
{   Write-Host "Adding the database $Name..."

    # Define the database options
    $NewDatabase = new-object ('Microsoft.SqlServer.Management.Smo.Database') ($SQLServerObj, $Name)    
    $NewDatabase.Collation = "Latin1_General_CI_AS_KS_WS"
    $NewDatabase.RecoveryModel = "Full"
    $NewDatabase.CompatibilityLevel = 100
    $NewDatabase.AutoCreateStatisticsEnabled = $true 
    $NewDatabase.AutoUpdateStatisticsEnabled = $true

    # Create the filegroup and primary data file
    $PrimaryFileGroup = new-object ('Microsoft.SqlServer.Management.Smo.FileGroup') ($NewDatabase, 'PRIMARY')
    $NewDatabase.FileGroups.Add($PrimaryFileGroup)

    $DataFileName = $Name + "_mdf1"
    $DataFile = new-object ('Microsoft.SqlServer.Management.Smo.DataFile') ($PrimaryFileGroup, $DataFileName)
    $DataFile.FileName = "K:\DATA\" + $DataFileName + ".mdf"
    $DataFile.Size = 10240 # 10 MB
    $DataFile.GrowthType = "KB"
    $DataFile.Growth = 25600 # 25 MB
    $DataFile.IsPrimaryFile = $true
    $PrimaryFileGroup.Files.Add($DataFile)

    # Create the log file
    $LogFileName = $Name + "_log"
    $LogFile = new-object ('Microsoft.SqlServer.Management.Smo.LogFile') ($NewDatabase, $LogFileName)
    $LogFile.FileName = "L:\LOGS\" + $LogFileName + '.ldf'
    $LogFile.Size = 4096 # 4 MB
    $LogFile.GrowthType = "KB"
    $LogFile.Growth = 10240 # 10 MB
    $NewDatabase.LogFiles.Add($LogFile)

    # Create the new database
    try{ $NewDatabase.Create() }
    catch { Write-Host "ERROR during the database creation." -foregroundcolor Red; $Error[0].Exception.Message } 

    $NewDatabase = $SQLServerObj.Databases | ?{$_.name -eq $Name }
    if($NewDatabase -ne $null)
    {   Write-Host "Changing the database owner and adding database permissions for the farm admin..."
        $NewDatabase.SetOwner("sa")
        $NewDatabase.Alter()

        # Add the farm admin to the db_owner role of the database
        $FarmAdmin = (Get-SPWebApplication -IncludeCentralAdministration | ?{$_.IsAdministrationWebApplication -eq $true }).ApplicationPool.Username
        $DBUser = New-Object ('Microsoft.SqlServer.Management.Smo.User') ($NewDatabase, $FarmAdmin)
        $DBUser.Login = $FarmAdmin
        $DBUser.Create()
        $DBRole = $NewDatabase.Roles["db_owner"]
        $DBRole.AddMember($FarmAdmin)  
        $NewDatabase 
    }  
}
else { Write-Host "A database with the name $Name is already created." }
}
