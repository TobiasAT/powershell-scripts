<#  
    .DESCRIPTION  
    Break the permissions on web, list or item level.    

    .Link 
    Read the description and updates at http://go.topedia.com/9Gqvi.

   .Author: Tobias Asböck
   .Updated: 19.07.2016 
#>  

Function Stop-SPPermissions
{ param (
[parameter(Mandatory=$true)]
[string]$WebUrl,

[parameter(Mandatory=$true)]
[ValidateSet('Web','List','Item', ignorecase=$False)]
[string]$Level,
[string]$ItemName,
[string]$Listname,
[switch]$CopyPermissions
)

$Web = Get-SPWeb $WebUrl
if($Level -eq "Item")
{   if($ItemName -ne $null -and $Listname -ne $null)
	{   if( $Web.Lists[$Listname].BaseType -ne "DocumentLibrary" )
		{ $Item = $Web.Lists[$Listname].Items | ?{$_.title -eq $ItemName } }
		else { $Item = $Web.Lists[$Listname].Items | ?{$_.Name -eq $ItemName } }
		
		if($Item -ne $null)
		{ $Item.BreakRoleInheritance($CopyPermissions); $Item.Update() }
	}
}
elseif($Level -eq "List")
{   if($Listname -ne $null)  
	{ $Web.Lists[$Listname].BreakRoleInheritance($CopyPermissions) }
}
else 
{ $web.BreakRoleInheritance($CopyPermissions); $web.Update() }
}
