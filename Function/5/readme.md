Break the permissions on web, list or item level.
-
#### **Command**   
Stop-SPPermissions

#### Compatibility  	
- SharePoint 2010+
- PowerShell 2+

#### **PARAMETER WebUrl**
The url of the website. The parameter is required.

#### **PARAMETER Level**
The permission level, valid values are Web, List or Item. The parameter is required.

#### **PARAMETER Listname**
The name of the list. Required for list or item level permissions.

#### **PARAMETER ItemName**
The name of the item. Only required for item level permissions.

#### **PARAMETER CopyPermissions**
Copy the existing permissions. Exclude the parameter to assign only the current user to security roles. 

#### **Example**   
Stop-SPPermissions -Level "Item" -WebUrl $Web.url -Listname "Library" -ItemName "Item 5"
