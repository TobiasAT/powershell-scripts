<#  
    .DESCRIPTION  
    Load an assembly from the GAC directory.

    .Link
    Read the description and updates at http://go.topedia.com/5CTGt.

   .Author: Tobias Asböck
   .Updated: 18.08.2016    
#>  


function Use-Assembly
{ param(
[parameter(Mandatory=$true)]
[string]$Name
)

    # Try to get the assembly from the .NET assembly library
    $AssemblyRootPath = "C:\WINDOWS\Microsoft.Net\assembly\GAC_MSIL\"
    $AssemblyPath = @(Get-ChildItem -Name $Name -Path $AssemblyRootPath -Recurse -ErrorAction SilentlyContinue | Sort Name)[0]

    # If the assembly is not part of .NET fall back to the assembly library
    if($AssemblyPath -eq $null)
    {   $AssemblyRootPath = "C:\WINDOWS\assembly\GAC_MSIL\" 
        $AssemblyPath = @(Get-ChildItem -Name $Name -Path $AssemblyRootPath -Recurse -ErrorAction SilentlyContinue | Sort Name)[0]
    }

    # Load the assembly
    if($AssemblyPath -ne $null)
    {  Add-Type -Path ($AssemblyRootPath + $AssemblyPath) }
    else 
    { Write-Host "Assembly '$Name' is not available." -foregroundcolor Red }
}
