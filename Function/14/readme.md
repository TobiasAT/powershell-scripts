Load an assembly from the GAC directory.
-
#### **Command**   
Use-Assembly

#### Compatibility (tested) 	
- PowerShell 4

#### **PARAMETER Name**
The name of the DLL file (e.g. Microsoft.SqlServer.Smo.dll). The parameter is required.

#### **Example**   
Use-Assembly -Name "Microsoft.SqlServer.Smo.dll"
