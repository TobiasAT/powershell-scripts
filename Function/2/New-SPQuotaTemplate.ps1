<#  
    .DESCRIPTION  
    Add a new quota template to the SharePoint central administration. 

    .Link 
    Read the description and updates at http://go.topedia.com/1VKJY.

   .Author: Tobias Asböck
   .Updated: 19.07.2016 
#>  

Function New-SPQuotaTemplate
{ param([Parameter(Mandatory=$true)][string]$Name,
[Parameter(Mandatory=$true)][Int64]$StorageMaximumLevel, 
[Int64]$StorageWarningLevel = 0, 
[Int]$UserCodeMaximumLevel = 300, 
[Int]$UserCodeWarningLevel = 0
    )
# Validate that the template is not already created     
$CAService = [Microsoft.SharePoint.Administration.SPWebService]::ContentService
if($CAService.QuotaTemplates[$Name] -eq $null)
{
    # Set the Properties #  
    $Quota = New-Object Microsoft.SharePoint.Administration.SPQuotaTemplate
    $Quota.Name = $Name
    $Quota.StorageMaximumLevel = $StorageMaximumLevel
    if($StorageWarningLevel -gt $StorageMaximumLevel)
    { $StorageWarningLevel = 0 ; Write-Host "Storage warning level is higher than storage max level, warning level removed."   }
    $Quota.StorageWarningLevel = $StorageWarningLevel
    $Quota.UserCodeMaximumLevel = $UserCodeMaximumLevel
    if($UserCodeWarningLevel -gt $UserCodeMaximumLevel)
    { $UserCodeWarningLevel = 0; Write-Host "User code warning level is higher than user code max level, warning level removed." }
    $Quota.UserCodeWarningLevel = $UserCodeWarningLevel        
    
    Write-Host "Adding the quota template $Name..."
    $CAService.QuotaTemplates.Add($Quota)    
    $CAService.Update()
}
else { Write-Host "'$Name' is already in the quota templates. Please use Update-SPQuotaTemplate to change the template." }
}
