Add a new quota template to the SharePoint central administration 
-
#### **Command**   
New-SPQuotaTemplate

#### Compatibility  	
- SharePoint 2010+
- PowerShell 2+

#### **PARAMETER Name**
Defines the template name.

#### **PARAMETER StorageMaximumLevel**
The max. storage level, in bytes. The parameter is required.

#### **PARAMETER StorageWarningLevel**
The quota warning level, in bytes. Add 0 for no warning (default value).

#### **PARAMETER UserCodeMaximumLevel**
The max. points for sandbox code. The default value is 300.

#### **PARAMETER UserCodeWarningLevel**
The warning level (in points) for sandbox code. Add 0 for no warning (default value).

#### **Example:**   
New-SPQuotaTemplate -Name "Site Quota 5000 MB" -StorageMaximumLevel 5368709120 -StorageWarningLevel 4294967296
