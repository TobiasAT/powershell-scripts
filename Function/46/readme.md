Enable an additional license for an Office 365 account
-  
Note, the function enables the license only if the parent plan is already enabled.    

#### **Command**   
Enable-O365License

#### Compatibility (tested) 	
- PowerShell 4
- Office 365

#### **PARAMETER UPN**
The User Principal Name of the user. The parameter is required.  

#### **PARAMETER LicenseName**
The internal license name of Office 365. The parameter is required.

For an example list of internal names read https://blogs.technet.microsoft.com/treycarlee/2014/12/09/powershell-licensing-skus-in-office-365.  

#### **PARAMETER LicensePlan**
Enable the license for a specific plan. Valid values are E3, E4, E5, E1 or All.

The default option is to enable the license in all active plans, but only if the license is available in more than one plan.

#### **Example**   
Enable-O365License -UPN [UserPrincipalName] -LicenseName "PROJECTWORKMANAGEMENT" -LicensePlan "E3"
