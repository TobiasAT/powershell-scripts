
<#  
    .DESCRIPTION  
    Enable an additional license for an Office 365 account.
	Note, the function enables the license only if the parent plan is already enabled. 

    .Link 
    Read the description and updates at http://go.topedia.com/dhVsP.

   .Author: Tobias Asböck
   .Updated: 27.09.2017
#> 


function Enable-O365License
{ param ( [parameter(Mandatory=$true)][string]$UPN, 
	[parameter(Mandatory=$true)][string]$LicenseName,

	[ValidateSet('All','E3','E4','E5','E1')] 
	[string]$LicensePlan = 'All'
  )

$UPN = $UPN.tolower()

# Receive the user account
$O365User = Get-MsolUser -UserPrincipalName $UPN -ErrorAction SilentlyContinue

if($O365User -ne $null)
{   # Check the current license and whether the new license is part of the plan.
	Write-Host "Validating the current license..."
	
	if($LicensePlan -eq 'E3' )
	{ $AllO365AccountSkus = ($O365User.Licenses | ?{$_.AccountSkuId -like "*:ENTERPRISEPACK" }).AccountSkuId }
	elseif($LicensePlan -eq 'E4' )
	{ $AllO365AccountSkus = ($O365User.Licenses | ?{$_.AccountSkuId -like "*:ENTERPRISEWITHSCAL" }).AccountSkuId  }
	elseif($LicensePlan -eq 'E5' )
	{ $AllO365AccountSkus = ($O365User.Licenses | ?{$_.AccountSkuId -like "*:ENTERPRISEPREMIUM" }).AccountSkuId  }
	elseif($LicensePlan -eq 'E1' )
	{ $AllO365AccountSkus = ($O365User.Licenses | ?{$_.AccountSkuId -like "*:STANDARDPACK" }).AccountSkuId  }
	else
	{ $AllO365AccountSkus = ($O365User.Licenses | ?{$_.ServiceStatus.ServicePlan.Servicename -eq $Licensename } | sort AccountSkuId ).AccountSkuId }
	
	if($AllO365AccountSkus.Count -gt 0)		
	{   # Save the current disabled plans		
		foreach($O365AccountSku in $AllO365AccountSkus)
		{   if($AllO365AccountSkus.Count -gt 1)
			{   Write-Host ""; Write-Host ("AccountSku " + $O365AccountSku + ":") -f yellow }
			
			$CurrentDisabledPlans = ($O365User.Licenses | ?{$_.AccountSkuId -eq $O365AccountSku }).ServiceStatus  | ?{$_.ProvisioningStatus -eq 'Disabled' } 
			if(($CurrentDisabledPlans | ?{$_.Serviceplan.Servicename -eq $Licensename }).Count -gt 0)
			{   # Exclude the new license from the disabled plans.							
				Write-Host "Enabling the license $Licensename for $UPN..."
				$CustomDisabledPlans = @()
				$CustomDisabledPlans = $CurrentDisabledPlans.ServicePlan | ?{$_.ServiceName -ne $Licensename } | select -ExpandProperty servicename

				# Prepare an updated licence plan 
				$NewUserPlan = New-MsolLicenseOptions -AccountSkuId $O365AccountSku -DisabledPlans $CustomDisabledPlans
				
				# Assign the new plan to the user account
				Set-MsolUserLicense -UserPrincipalName $UPN –LicenseOptions $NewUserPlan
				Start-Sleep -Seconds 2				
			}
			else
			{ Write-Host "$Licensename is already active for $UPN, no update required." }		
		}	
	}
	else
	{   if($LicensePlan -eq 'All')
		{ Write-Host "$UPN has no license plan for the license type $Licensename assigned." -f red }
		else
		{ Write-Host "$UPN has no $LicensePlan license plan assigned." -f red   }	
	}
}
else
{ Write-Host "$UPN is not available in Office 365." -f red   }


}
