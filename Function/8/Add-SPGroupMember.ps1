<#  
    .DESCRIPTION  
    Add an AD user or group to a SharePoint group.

    .Link 
    Read the description and updates at http://go.topedia.com/gOYm2.

   .Author: Tobias Asböck
   .Updated: 26.07.2016 
#> 

Function Add-SPGroupMember
{ param(
[parameter(Mandatory=$true)]
[string]$Groupname,

[parameter(Mandatory=$true)]
[string]$WebUrl,

[parameter(Mandatory=$true)]
[string]$Username
)

$Web = Get-SPWeb $WebUrl -erroraction silentlycontinue
if($Web -ne $null)
{   $SPGroup = $Web.SiteGroups[$Groupname]

    if($SPGroup -ne $null)
    {   $WebUser = Get-SPUser $Username -Web $Web -erroraction silentlycontinue

        if($WebUser -eq $null)
        { $WebUser = New-SPUser -UserAlias $Username -Web $Web }

        $SPGroup.AddUser($WebUser) 
        $SPGroup.Update()
    }
    else { Write-Host "No group '$Groupname' available." -foregroundcolor Red }
}
else { Write-Host "No website at $WebUrl available." -foregroundcolor Red }
}

