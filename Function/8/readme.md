Add an AD user or group to a SharePoint group.
-
#### **Command**   
Add-SPGroupMember

#### Compatibility  	
- SharePoint 2010+ 
- PowerShell 2+

#### **PARAMETER Groupname**
The name of the group. The parameter is required.

#### **PARAMETER WebUrl**
The URL of the website. The parameter is required.

#### **PARAMETER Username**
The username or AD group. The parameter is required.

#### **Example**   
Add-SPGroupMember -Groupname "Visitors" -WebUrl "https://community.topedia.com/team1" -Username "NT AUTHORITY\authenticated users"
