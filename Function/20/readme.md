Get a SharePoint list and the items from that list.
-
#### **Command**   
Get-SPList

#### Compatibility (tested) 	
- PowerShell 4
- SharePoint 2013

#### **PARAMETER ListUrl**
Url of the SharePoint list. The parameter is required.

#### **Example**   
Get-SPList -ListUrl  "https://community.topedia.com/lists/SPOUsers/AllItems.aspx" 



