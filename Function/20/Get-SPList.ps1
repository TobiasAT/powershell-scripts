<#  
    .DESCRIPTION  
    Get a SharePoint list and the items from that list.

    .Link
    Read the description and updates at http://go.topedia.com/EMsIe.

   .Author: Tobias Asböck
   .Updated: 23.08.2016    
#>  

function Get-SPList
{ param (
    [parameter(Mandatory=$true)]
    [string]$ListUrl
    )

try { $site = New-Object Microsoft.SharePoint.SPSite($ListUrl) }
catch { Write-Host "Url $ListUrl not available." -foregroundcolor Red; return  }

$web = $site.OpenWeb()
try{ $web.GetList($listURL) }
catch { Write-Host "The list is not available." -foregroundcolor Red; $web.dispose(); $site.Dispose(); return }

$web.dispose()
$site.Dispose()
}
