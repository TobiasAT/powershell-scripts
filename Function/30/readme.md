Modify a SharePoint quota template. 
-
#### **Command**   
Update-SPQuotaTemplate

#### Compatibility (tested) 	
- PowerShell 4
- SharePoint 2013

#### **PARAMETER StorageMaximumLevel**
The max. storage level, in bytes. The parameter is required.

#### **PARAMETER StorageWarningLevel**
The quota warning level, in bytes. Add 0 for no warning. The parameter is required. 

#### **PARAMETER UserCodeMaximumLevel**  
The max. points for sandbox code. The default value is 300.
   
#### **PARAMETER UserCodeWarningLevel**  
The warning level (in points) for sandbox code. Add 0 for no warning. The parameter is required.

#### **Example**   
Update-SPQuotaTemplate -Name "Community" -StorageMaximumLevel 5368709120 -StorageWarningLevel 4294967296 -UserCodeWarningLevel 0
