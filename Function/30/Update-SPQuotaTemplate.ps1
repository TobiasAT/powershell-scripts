
<#  
	.DESCRIPTION  
    Modify a quota template. 

    .Link 
    Read the description and updates at http://go.topedia.com/vyOUL.

   .Author: Tobias Asböck - BBF30
   .Updated: 13.12.2016  
#>  


function Update-SPQuotaTemplate
{ param([Parameter(Mandatory=$true)][string]$Name,
[Parameter(Mandatory=$true)][Int64]$StorageMaximumLevel, 
[Parameter(Mandatory=$true)][Int64]$StorageWarningLevel, 
[Int]$UserCodeMaximumLevel = 300, 
[Parameter(Mandatory=$true)][Int]$UserCodeWarningLevel
    )

# Validate that the template is not already created     
$CAService = [Microsoft.SharePoint.Administration.SPWebService]::ContentService
$QuotaTemplate = $CAService.QuotaTemplates[$Name]

if($QuotaTemplate -ne $null)
{
    # Set the Properties #  
	Write-Host "Changing the quota template $Name..."   
    $QuotaTemplate.StorageMaximumLevel = $StorageMaximumLevel
    if($StorageWarningLevel -gt $StorageMaximumLevel)
    { $StorageWarningLevel = 0 ; Write-Host "Storage warning level is higher than storage max level, warning level removed."   }
    $QuotaTemplate.StorageWarningLevel = $StorageWarningLevel
    $QuotaTemplate.UserCodeMaximumLevel = $UserCodeMaximumLevel
    if($UserCodeWarningLevel -gt $UserCodeMaximumLevel)
    { $UserCodeWarningLevel = 0; Write-Host "User code warning level is higher than user code max level, warning level removed." }
    $QuotaTemplate.UserCodeWarningLevel = $UserCodeWarningLevel  
    $CAService.Update()
}
else { Write-Host "Quota template '$Name' is not available. Please use Add-SPQuotaTemplate to add a new template." }
}

