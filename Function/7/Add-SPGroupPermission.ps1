<#  
    .DESCRIPTION  
    Add permissions for a SharePoint group on web, list or item level.

    .Link 
    Read the description and updates at http://go.topedia.com/IUSeb.

   .Author: Tobias Asböck
   .Updated: 26.07.2016 
#> 

Function Add-SPGroupPermission
{ param (
[parameter(Mandatory=$true)]
[string]$Groupname,

[parameter(Mandatory=$true)]
[string]$WebUrl,

[parameter(Mandatory=$true)]
[ValidateSet('Web','List','Item', ignorecase=$False)]
[string]$Level,

[parameter(Mandatory=$true)]
[string]$PermissionRole,

[string]$ItemName,
[string]$Listname

)

$Web = Get-SPWeb $WebUrl -erroraction silentlycontinue
if($Web -ne $null)
{   $SCGroup = $Web.SiteGroups[$Groupname]

    if($SCGroup -ne $null)
    {   $Assignment = New-Object Microsoft.SharePoint.SPRoleAssignment($SCGroup)
        $RoleDefinition = $Web.RoleDefinitions[$PermissionRole]
        $Assignment.RoleDefinitionBindings.Add($RoleDefinition)

        if($Level -eq "Item")
        { if($ItemName -ne $null -and $Listname -ne $null)
            { if( $Web.Lists[$Listname].BaseType -ne "DocumentLibrary" )
                { $Item = $Web.Lists[$Listname].Items | ?{$_.title -eq $ItemName } }
                else { $Item = $Web.Lists[$Listname].Items | ?{$_.Name -eq $ItemName } }
                if($Item -ne $null)
                    { $Item.BreakRoleInheritance($true) 
                      $Item.RoleAssignments.Add($Assignment)
                      $Item.Update() 
                    }
            }
        }
        elseif($Level -eq "List")
        {   if($Listname -ne $null)  
            {   $List = $Web.Lists[$Listname]
                $List.RoleAssignments.Add($Assignment) 
            }
        }
        else 
        {   $Assignment.RoleDefinitionBindings.Add($RoleDefinition);
            $Web.RoleAssignments.Add($Assignment); $web.Update()
        }

    }
    else { Write-Host "No group '$Groupname' available." -foregroundcolor Red }
    $Web.Dispose()
}
else { Write-Host "No website at $WebUrl available." -foregroundcolor Red }
}  
