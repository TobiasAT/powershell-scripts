Add permissions for a SharePoint group on web, list or item level.
-
#### **Command**   
Add-SPGroupPermission

#### Compatibility  	
- SharePoint 2010+ 
- PowerShell 2+

#### **PARAMETER Groupname**
The name of the group. The parameter is required.

#### **PARAMETER WebUrl**
The URL of the website. The parameter is required.

#### **PARAMETER Level**
The permission level, valid values are web, list or item. The parameter is required.

#### **PARAMETER PermissionRole**
The permission role (e.g. Full Control, Contribute, Read,...). The parameter is required.

#### **PARAMETER Itemname**
The name of the item. Only required for item level permissions.

#### **PARAMETER Listname**
The name of the list. Required for list or item level permissions.

#### **Example**   
Add-SPGroupPermission -Level "List" -Groupname "Visitors"  -WebUrl "https://community.topedia.com/team1"  -PermissionRole "Read" -Listname "Tasks"
