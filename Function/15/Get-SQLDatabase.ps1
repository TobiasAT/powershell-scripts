<#  
    .DESCRIPTION  
    Get a database from a SQL server.

    .Link
    Read the description and updates at http://go.topedia.com/lY65I.

   .Author: Tobias Asböck
   .Updated: 18.08.2016    
#>  

function Get-SQLDatabase
{ param(
[parameter(Mandatory=$true)]
[string]$Name,

[parameter(Mandatory=$true)]
[string]$Server
)

# Check whether all commands are available
if (!(Get-Command Use-Assembly -errorAction SilentlyContinue))
	{ Write-Host "The command Use-Assembly is not available. Please load the command at http://go.topedia.com/5CTGt." -Foregroundcolor Red; return }

Use-Assembly -Name "Microsoft.SqlServer.Smo.dll"
$SQLServerObj = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Server -ArgumentList $Server
$Database = $SQLServerObj.Databases[$Name]  

if($Database -ne $null)
    { $Database }
else 
    { Write-Host "Database $Name not available." -foregroundcolor Red }

}              
