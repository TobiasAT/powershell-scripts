Get a database from a SQL server.
-
#### **Command**   
Get-SQLDatabase

#### Compatibility (tested) 	
- PowerShell 4
- SQL Server 2008
- SQL Server 2012

#### **PARAMETER Name**
Name of the database. The parameter is required.  

#### **PARAMETER Server**
Name of the SQL server. The parameter is required.  


#### **Example**   
Get-SQLDatabase -Name "Database1" -Server "SQL1"
